#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root (use sudo)" 1>&2
   exit 1
fi
is_Raspberry=$(cat /proc/device-tree/model | awk  '{print $1}')
if [ "x${is_Raspberry}" != "xRaspberry" ] ; then
  echo "Sorry, this drivers only works on raspberry pi"
  exit 1
fi

cd /home/pi

git clone https://github.com/respeaker/seeed-voicecard
cd  seeed-voicecard
./install.sh

cd /home/pi
git clone https://gitee.com/baozhu/aws
cd aws/aws-iot-device-sdk-python
sudo python setup.py install

cp /home/pi/aws/aws.service /lib/systemd/system/
sudo systemctl  enable aws