# aws

#### 介绍
aws demo
#### 安装教程

1. mcu端按照文档[WM_W60X_固件升级指导_V1.2.pdf](https://gitlab.com/baozhu/aws/blob/master/WM_W60X_%E5%9B%BA%E4%BB%B6%E5%8D%87%E7%BA%A7%E6%8C%87%E5%AF%BC_V1.2.pdf)

[烧录的固件下载](https://gitee.com/baozhu/aws/raw/master/WM_W600_GZ.img) ， 烧录完成以后如下图所示：

![固件烧录成功](https://images.gitee.com/uploads/images/2019/0226/175918_de5b153a_493118.png "mcu.png")

2，树莓派端执行
```sh
  curl https://gitlab.com/baozhu/aws/raw/master/install.sh | sudo bash
```

